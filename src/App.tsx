import './App.css'
import Board from './components/Board'
import UpcomingBlocks from './components/UpcomingBlocks';
import { useTetris } from './hooks/useTetris';

function App() {
  const { board, startGame, isPlaying, score, upcomingBlocks} = useTetris(); // upcomingBlocks not called = css problem

  return (

    <div className="app flex justify-center items-center bg-no-repeat bg-cover bg-center backdrop-blur-md">
      <div className="flex flex-col m-10 bg-white/40 p-10 rounded-t-xl border-2 border-black/50">
      <div className='mb-10 text-center'><span className="text-4xl">Tetris!</span></div>
      <div className='flex justify-center flex-row columns-2'>
        <div className='flex-wrap'>
          <Board currentBoard={board} />
        </div>
        <div className='flex flex-wrap align-center px-10 flex-col w-96'>
        <span className='text-center'>
          Score:
          </span>
          <span className='m-8 text-center'>
          {score}
          </span>
          {isPlaying ? (
          <div>  
          <div className='mb-10 text-center'>Use os botões direcionais do teclado para jogar.</div>
          <div className='mb-10 text-center text-sm'>Use the keyboard's directional buttons to play.</div>
          <UpcomingBlocks upcomingBlocks={upcomingBlocks}/>
          </div>
        ) : (
          <button className="bg-sky-700 py-3 px-5 rounded-md text-white" onClick={startGame}>New Game</button>
        )}
        </div>
      </div>
      </div> 
    </div>
  )
}

export default App
