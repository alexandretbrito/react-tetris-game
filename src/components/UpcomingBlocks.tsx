import { Block, SHAPES } from '../types/types';

interface Props {
  upcomingBlocks: Block[];
}

function UpcomingBlocks({ upcomingBlocks }: Props) {
  return (
    <div className="flex flex-col-reverse items-center">
      {upcomingBlocks.map((block, blockIndex) => {
        const shape = SHAPES[block].shape.filter((row) =>
          row.some((cell) => cell)
        );
        return (
          <div  key={blockIndex} className='flex justify-center items-center w-full m-3'>
          <div key={blockIndex}>
            {shape.map((row, rowIndex) => {
              return (
                <div key={rowIndex} className="flex flex-row">
                  {row.map((isSet, cellIndex) => {
                    const cellClass = isSet ? block : null;
                    return (
                      <div
                        key={`${blockIndex}-${rowIndex}-${cellIndex}`}
                        className={`celula ${cellClass}`}
                      ></div>
                    );
                  })}
                </div>
              );
            })}
          </div>
          </div>
        );
      })}
    </div>
  );
}

export default UpcomingBlocks;