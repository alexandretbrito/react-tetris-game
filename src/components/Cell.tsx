import { CellOptions } from "../types/types"

interface Props {
    type: CellOptions;
}

const Cell = ({type}: Props) => {
  return (
    <div className={`celula ${type}`}/>
  )
}

export default Cell
