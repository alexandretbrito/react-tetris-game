# React Tetris Game

A simple Tetris game written in ReactJS and TailwindCSS for practicing purposes. It's not mobile-friendly.

![image](img-tetris.jpg "The game")

The demo is [here](https://alexandrebrito.com/tetris-game/) 

Background image from Reading the Book Travel ([https://readingthebooktravel.com/](https://readingthebooktravel.com/))
